# Build System Status

## Integrated

- [x] cmake target for Linux X11 build
- [x] cmake target for Linux X11 TestSuite build
- [x] cmake target for PSP build
- [ ] cmake target for Android build
- [ ] cmake target for Windows build
- [ ] cmake target for Linux Qt4 build
- [ ] cmake target for Linux SDL build

## add post build actions

- [ ] create zip files with cmake (remove python scripts)
- [ ] create install target for linux
