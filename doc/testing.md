# Testing

## Code Coverage

### current line statistics

|Language         |files  |   code   |
|:----------------|------:|---------:|
|C++              |  173  |  76080   |
|C/C++ Header     |  184  |  20524   |
|C                |   11  |   3773   |
|Objective C      |    5  |   1154   |
|MSBuild script   |    1  |    175   |
|make             |    2  |     36   |
|**SUM:**         |**376**|**101742**|

### current code coverage

|            |Hit 	|Total 	|Coverage|
|:-----------|-----:|------:|-------:|
|Lines 	     |18931 |38283 	|49.5 %  |
|Functions   |2234 	|4142 	|53.9 %  |
|Branches    |21776 |76288 	|28.5 %  |


## Integration Testing

### TestSuite

Each Linux Debug build has the integrated test suite enabled. Currently this 
test suite is closely coupled with the UI.

![alt text](images/test_result.png)

### Headless Tests

To avoid regressions the build job needs to be coupled with a set of integrated tests.
The test should be full automated. This means they need to run with out any kind of human 
interaction.

- [ ] Headless test suite
- [ ] [approval tests](https://github.com/approvals/ApprovalTests.cpp)

