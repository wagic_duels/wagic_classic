# Wagic, the Homebrew (Classic Edition)

This Project is a reanimation from _Wagic the Homebrew_ based on the version 18.6 from 2012.
This is the last version with the [main developer involved](http://wololo.net/download/)
and it seems like the last version, where the psp build is full functional.

If you search for a more _up to date version_ of _Wagic_ take a look at the project's official 
[github](https://github.com/WagicProject/wagic).

![alt text](doc/images/menu_screen.png)

## License

This is a short summary of the licenses of the various parts of _Wagic, The Homebrew_.
More specific License texts can be found in each subfolder. In particular,
resource files (graphics, etc...) do **not** follow the same licenses as the source code.

* JGE is licensed under the [BSD License](./JGE/LICENSE.md)
* Wagic's source code is licensed under the [BSD License](./projects/mtg/LICENSE.md)
* Wagic's resources (graphics, sounds, etc...) use [various licenses](./projects/mtg/Res/LICENSE)

## Working Builds
[![Build status](https://ci.appveyor.com/api/projects/status/7x25u9uc52de345d/branch/legacy?svg=true)](https://ci.appveyor.com/project/zie8778791/wagic-classic/branch/legacy)
[![Build status](https://gitlab.com/wagic_duels/wagic_classic/badges/legacy/pipeline.svg)](https://gitlab.com/wagic_duels/wagic_classic/commits/legacy)


- [x] PSP
- [x] Android (builds but not tested)
- [x] X11 (Linux)
- [x] X11 TestSuite (Linux)
- [ ] Qt4 QML Version (builds but breaks on run)
- [ ] Qt4 QtWidget Version (builds but breaks on run)
- [ ] SDL1.2 (doesn't build on Linux)
- [x] Windows (tested with [Wine](https://www.winehq.org/))
- [ ] Glut (not tested)
- [ ] iOS (not tested)
- [ ] MacOSX (not tested)

## Tasks

### continuous integration

- [x] continuous integration for PSP build
- [x] continuous integration for Android build
- [x] continuous integration for Linux X11 build
- [ ] continuous integration for Linux Qt4 build
- [ ] continuous integration for Linux SDL build
- [x] continuous integration for Windows build ([AppVeyor][1])

### testing

- [ ] build with integration [tests](./doc/testing.md)
- [ ] generate [coverage](./doc/testing.md) report **(wip)**
- [ ] static code analysis (at least cpp check)
- [ ] generate documentations

### continuous deployment

- [ ] continuous deployment for PSP
- [ ] continuous deployment for Android
- [ ] continuous deployment for Linux
- [ ] continuous deployment for Windows

### modernize

- [ ] integrate a [build system](./doc/cmake.md) for all targets **(wip)**
- [ ] implement install targets
- [ ] better dependency management **(wip)**
- [ ] remove [out dated / not maintainable][./doc/targets.md] build targets (e.g. Maemo)


[1]: https://ci.appveyor.com/project/zie8778791/wagic-classic/branch/legacy