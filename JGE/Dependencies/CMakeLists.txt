
if( PSP )
    add_subdirectory(psp)
elseif( ANDROID )
    add_subdirectory(android)
    add_subdirectory(SDL)
elseif( UNIX )
    add_subdirectory(linux)
endif()